from time import sleep
from random import randint
from ppds import Thread, Mutex, Semaphore, print, Event


class SimpleBarrier:
    def __init__(self):
        self.M = Mutex()
        self.fibonacci = [0, 1]

    def barrier(self):
        self.M.lock()
        last = self.fibonacci[len(self.fibonacci) - 1]
        preLast = self.fibonacci[len(self.fibonacci) - 2]
        self.fibonacci.append(last + preLast)
        print("fibonacci " + str(self.fibonacci))

        self.M.unlock()


def barrier(barrier, thread_name):
    print('before: %s\n' % thread_name)
    barrier.barrier()
    print('after: %s\n' % thread_name)


simpleBarrier = SimpleBarrier()

threads = list()
for i in range(10):
    t = Thread(barrier, simpleBarrier, 'Thread %d' % i)
    threads.append(t)

for t in threads:
    t.join()
