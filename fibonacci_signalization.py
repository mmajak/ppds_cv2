from time import sleep
from random import randint
from ppds import Thread, Mutex, Semaphore, print, Event


class SimpleBarrier:
    def __init__(self):
        self.S = Semaphore(0)
        self.M = Mutex()
        self.fibonacci = [0, 1]
        self.count = 0

    def barrier(self, thread_id):
        self.M.lock()
        self.count += 1
        if (self.count == 1):
            self.S.signal()
        self.M.unlock()

        self.S.wait()
        print(thread_id + " starts fibonnaci count")
        last = self.fibonacci[len(self.fibonacci) - 1]
        preLast = self.fibonacci[len(self.fibonacci) - 2]
        self.fibonacci.append(last + preLast)
        print("fibonacci " + str(self.fibonacci))
        print(thread_id + " stops fibonnaci count")
        self.S.signal()


def barrier(barrier, thread_name):
    print('before: %s\n' % thread_name)
    barrier.barrier(thread_name)
    print('after: %s\n' % thread_name)


simpleBarrier = SimpleBarrier()

threads = list()
for i in range(10):
    t = Thread(barrier, simpleBarrier, 'Thread %d' % i)
    threads.append(t)

for t in threads:
    t.join()
