from time import sleep
from random import randint
from ppds import Thread, Mutex, Semaphore, print, Event


def rendezvous(thread_name):
    sleep(randint(1, 10) / 10)
    print('rendezvous: %s\n' % thread_name)


def ko(thread_name):
    print('ko: %s\n' % thread_name)
    sleep(randint(1, 10) / 10)


class SimpleBarrier:
    def __init__(self, N):
        self.N = N
        self.M = Mutex()
        self.count = 0
        self.e = Event()

    def barrier(self):
        self.M.lock()
        self.count += 1

        if (self.count == self.N):
            self.e.signal()
            sleep(randint(5, 10) / 10)

        self.M.unlock()
        self.e.wait()

        self.M.lock()
        self.count -= 1
        if (self.count == 0):
            self.e.clear()
        self.M.unlock()


"""
Kazde vlakno vykonava kod funkcie 'barrier'.
Doplnte synchronizaciu tak, aby sa vsetky vlakna pockali
nielen pred vykonanim funkcie 'ko', ale aj
*vzdy* pred zacatim vykonavania funkcie 'rendezvous'.
"""


def barrier(barrier, barrier2, thread_name):
    while True:
        # ...
        rendezvous(thread_name)
        barrier.barrier()

        ko(thread_name)
        barrier2.barrier()


"""
Vytvorime vlakna, ktore chceme synchronizovat.
Nezabudnime vytvorit aj zdielane synchronizacne objekty,
a dat ich ako argumenty kazdemu vlaknu, ktore chceme pomocou nich
synchronizovat.
"""

simpleBarrier = SimpleBarrier(10)
simpleBarrier2 = SimpleBarrier(10)

threads = list()
for i in range(10):
    t = Thread(barrier, simpleBarrier, simpleBarrier2, 'Thread %d' % i)
    threads.append(t)

for t in threads:
    t.join()
