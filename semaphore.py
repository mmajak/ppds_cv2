from ppds import Thread, Semaphore, Mutex, print, Event
from time import sleep
from random import randint


# Semaphore.signal() inkrementuje hodnotu sempfora
# Sempahore.wait() dekrementuje hodnotu semafora
# pri vytvarani semafora nastavis semaforu vahu - kolko krat bude volanie waitu() neblokujuce
# pri nastaveni semafora na 0 sa po zavolani waitu caka donekonecna
# pri nastaveni semafora na 1 prve zavolanie waitu nie je blokujuce, az druhe
# Mutex je semafor inicializovany na 1


class Shared:
    def __init__(self, N):
        self.N = N
        self.t = Semaphore(1)
        self.count = 0

    def func(self):
        self.t.wait()
        self.count += 1
        if (self.count != self.N):
            self.t.signal()


# vytvorim max size semafora
t = Shared(5)


# predpokladajme, ze nas program vytvara a spusta 5 vlakien,
# ktore vykonavaju nasledovnu funkciu, ktorej argumentom je
# zdielany objekt jednoduchej bariery
def example(shared, thread_id):
    sleep(randint(1, 10) / 10)
    print("vlakno %d pred funkciou" % thread_id)
    shared.func()
    print("vlakno %d po funkcii" % thread_id)


children = [Thread(example, t, i) for i in range(5)]
for child in children:
    child.join()
