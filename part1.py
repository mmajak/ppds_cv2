from ppds import Thread, Semaphore, Mutex, print, Event
from time import sleep
from random import randint


# ked sa naplni kapacita bariery, signal odblokuje vsetky pozastavene thready

class SimpleBarrier:
    def __init__(self, N):
        self.N = N
        self.M = Mutex()
        self.count = 0
        self.e = Event()

    def barrier(self):
        self.M.lock()
        self.count += 1

        if (self.count == self.N):
            self.e.signal()
        self.M.unlock()
        self.e.wait()

# nastavenie kapacity bariery
t = SimpleBarrier(5)


# predpokladajme, ze nas program vytvara a spusta 5 vlakien,
# ktore vykonavaju nasledovnu funkciu, ktorej argumentom je
# zdielany objekt jednoduchej bariery
def barrier_example(barrier, thread_id):
    sleep(randint(1, 10) / 10)
    print("vlakno %d pred barierou" % thread_id)
    barrier.barrier()
    print("vlakno %d po bariere" % thread_id)


children = [Thread(barrier_example, t, i) for i in range(5)]
for child in children:
    child.join()